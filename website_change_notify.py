import json
import smtplib
import requests
import hashlib
from email.message import EmailMessage
from email.parser import Parser
from email.policy import default
import re
import time
import random
from werkzeug.utils import secure_filename
import datetime, pytz

def run():
    with open("storage.json", "r+") as f:
        data = json.load(f)
    with open("config.json", "r") as f:
        conf = json.load(f)
    
    urls = data.get("hashes")
    for url in urls:
        stored_hash = urls.get(url)
        resp = requests.get(url)
        if resp.status_code != 200: continue
        calculated_hash = hashlib.md5(resp.text.encode("utf-8")).hexdigest()
        if calculated_hash != stored_hash:
            with open("storage.json", "w+") as f:
                data["hashes"][str(url)] = calculated_hash
                json.dump(data, f)
            diff_filename = secure_filename(f"{calculated_hash}_{url}_{str(datetime.datetime.now(pytz.timezone('Europe/Berlin')))}.html")
            with open(f"diffs/{diff_filename}", "w+") as f:
                f.write(resp.text)
            
            match = re.search(r"https?:\/\/(?:www\.)?([-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b)*(\/[\/\d\w\.-]*)*(?:[\?])*(.+)*", url)
            domain_name = match.group(1)
            headers = Parser(policy=default).parsestr(
            f'From: Website Change Notifier <{conf["smtp"]["from_address"]}>\n'
            f'To: <{conf["smtp"]["to_address"]}>\n'
            f'Subject: Website ({domain_name}) has changed!\n'
            '\n'
            f'{url} has changed!\nOld md5 hash: {stored_hash}\nNew md5 hash: {calculated_hash}')
            s = smtplib.SMTP(conf["smtp"]["domain"], conf["smtp"]["port"])
            s.starttls()
            s.login(conf["smtp"]["from_address"], conf["smtp"]["password"])
            s.sendmail(conf["smtp"]["from_address"], conf["smtp"]["to_address"], headers.as_string().encode("utf-8"))
            s.quit()
            time.sleep(random.randint(1, 10))
           

while True:    
    run()
    time.sleep(random.randint(30, 180))
